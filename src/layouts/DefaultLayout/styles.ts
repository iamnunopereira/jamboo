import styled from '@emotion/styled'

export const Container = styled.div`
  display: grid;
  grid-template-columns: 30rem auto;
  max-width: 192rem;
  max-height: 108rem;

  @media only screen and (max-width: 1024px) {
    grid-template-columns: auto;
  }
`
