import GDPR from '@/features/GDPR'
import Header from '@/features/Header'
import Metadata from '@/components/Metadata'
import LanguageSelector from '@/components/LanguageSelector'
import { Container } from './styles'
import { PropsWithChildren } from '@/types'
import { useSettingsContext } from '@/hooks/useSettingsContext'

// Common Responsive Breakpoints
//
// Mobile: 360 x 640.
// Mobile: 375 x 667.
// Mobile: 360 x 720.
// iPhone X: 375 x 812.
// Pixel 2: 411 x 731.
// Tablet: 768 x 1024.
//
// Laptop: 1366 x 768.
// High-res laptop or desktop: 1920 x 1080.

const DefaultLayout = ({ children }: PropsWithChildren) => {
  const { gdpr } = useSettingsContext()

  return (
    <>
      <Metadata />
      <LanguageSelector />
      <Container>
        <Header />
        <main>{children}</main>
      </Container>
      {gdpr && <GDPR />}
    </>
  )
}

export default DefaultLayout
