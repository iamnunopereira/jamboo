import nookies from 'nookies'
import React, { useState, useEffect, createContext } from 'react'
import { auth } from '@/lib/firebaseClient'
import { useRouter } from 'next/router'
import { usePostHog } from 'posthog-js/react'
import { FirebaseError } from 'firebase/app'
import { PropsWithChildren } from '@/types'
import { onAuthStateChanged, User, GoogleAuthProvider, signInWithPopup, signOut } from 'firebase/auth'
import { ContextValue, AuthError } from './types'

export const AuthContext = createContext<ContextValue>({
  csUser: null,
  error: null,
  logOut: undefined,
  loginWithGoogle: undefined,
})

export const AuthContextProvider = ({ children }: PropsWithChildren) => {
  const [error, setError] = useState<null | AuthError>(null)
  const [user, setUser] = useState<null | User>(null)

  const provider = new GoogleAuthProvider()
  const posthog = usePostHog()
  const router = useRouter()

  const loginWithGoogle = async () => {
    setError(null)

    try {
      await signInWithPopup(auth, provider)
    } catch (error: FirebaseError | unknown) {
      if (error instanceof FirebaseError) {
        setError({ errorCode: error.code, errorMessage: error.message })
      }
    }
  }

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, async (user) => {
      if (user) {
        setUser(user)
        const token = await user.getIdToken()
        posthog!.identify(user.email!)

        nookies.set(null, 'token', token, {
          maxAge: 10 * 24 * 60 * 60,
          path: '/',
        })
      } else {
        setUser(null)
        posthog!.reset()

        nookies.destroy(null, 'token')
      }

      router.push('/')
    })

    return () => unsubscribe()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <AuthContext.Provider
      value={{
        csUser: user,
        error: error,
        logOut: signOut(auth),
        loginWithGoogle,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
