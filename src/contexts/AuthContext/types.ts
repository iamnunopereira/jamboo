import { User } from 'firebase/auth'

export type AuthError = {
  errorCode: string
  errorMessage: string
}

export type ContextValue = {
  csUser: User | null
  error: AuthError | null
  logOut?: Promise<void>
  loginWithGoogle?: () => Promise<void>
}
