import React, { useState, createContext } from 'react'
import { PropsWithChildren } from '@/types'
import { SettingsContextValueTypes } from './types'

export const SettingsContext = createContext<SettingsContextValueTypes>({
  gdpr: true,
  toggleGdpr: undefined,
})

export const SettingsContextProvider = ({ children }: PropsWithChildren) => {
  const [gdpr, toggleGdpr] = useState<boolean>(true)

  return (
    <SettingsContext.Provider
      value={{
        gdpr,
        toggleGdpr,
      }}
    >
      {children}
    </SettingsContext.Provider>
  )
}
