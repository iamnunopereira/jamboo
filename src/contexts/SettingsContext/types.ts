import { Dispatch, SetStateAction } from 'react'

export type SettingsContextValueTypes = {
  gdpr: boolean
  toggleGdpr?: Dispatch<SetStateAction<boolean>>
}
