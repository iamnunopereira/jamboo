import localFont from 'next/font/local'

export const californiaParadise = localFont({ src: '../../../public/California-Paradise.otf' })

export const garet = localFont({
  src: [
    {
      path: '../../../public/Garet-Book.otf',
      weight: '400',
      style: 'normal',
    },
    {
      path: '../../../public/Garet-Heavy.otf',
      weight: '600',
      style: 'normal',
    },
  ],
})
