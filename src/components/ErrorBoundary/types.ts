import { ElementType, HTMLAttributes, ReactNode } from 'react'

export type ErrorBoundaryProps = {
  children: ReactNode
  debug?: boolean
  fallback?: ElementType
  onError?: (_prop: string, _prop1: string) => void
} & HTMLAttributes<HTMLElement>

export type ErrorBoundaryState = {
  error?: string | null
  errorInfo?: {
    componentStack?: string
  } | null
}
