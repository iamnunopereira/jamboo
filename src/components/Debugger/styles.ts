import styled from '@emotion/styled'

export const Container = styled.div`
  max-width: 144rem;
  color: var(--color-base-black);
  font-size: 1.4rem;
  margin-block: 2.4rem;
`

export const Details = styled.details`
  white-space: pre-wrap;
`

export const Message = styled.p`
  color: var(--color-error-one);
  font-weight: bold;
  padding-block: 1.2rem;
`

export const Summary = styled.summary`
  max-width: fit-content;
  outline: none;
  cursor: pointer;
`

export const Title = styled.h1`
  margin: 0;
  font-size: 1.5rem;
`
