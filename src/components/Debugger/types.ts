export type DebuggerPropTypes = {
  error?: string
  errorStack?: string
}
