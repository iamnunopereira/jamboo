import styled from '@emotion/styled'

export const Container = styled.div`
  position: absolute;
  top: var(--spacers-c24);
  right: var(--spacers-c24);
  z-index: var(--stack-order-low);

  img {
    border: 3px solid var(--color-base-light-grey);
    border-radius: 100%;
  }
`
