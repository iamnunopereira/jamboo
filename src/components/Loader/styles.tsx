import styled from '@emotion/styled'

export const Container = styled.div`
  width: 27.8rem;
  height: 10.8rem;
  margin: 7rem auto;
  overflow: hidden;
  position: relative;
`

export const Box = styled.div`
  width: 50%;
  height: 50%;
  background: transparent;
  position: absolute;
  bottom: 0;
  right: 0;
  transform-origin: 15% 60%;
  animation: loading-2 2.1s ease-in-out infinite;

  @keyframes loading-2 {
    0% {
      transform: rotate(0deg);
    }
    5% {
      transform: rotate(-27deg);
    }
    30%,
    50% {
      transform: rotate(0deg);
    }
    55% {
      transform: rotate(27deg);
    }

    83.3% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(0deg);
    }
  }
`

export const BoxInnerOne = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  transform-origin: 15% 60%;
  animation: loading-3 2.1s ease-in-out infinite;

  @keyframes loading-3 {
    0% {
      transform: rotateY(0deg);
    }
    50% {
      transform: rotateY(180deg);
    }
    100% {
      transform: rotateY(0deg);
    }
  }
`

export const BoxOne = styled.div`
  width: 7rem;
  height: 1.7rem;
  background: #333;
  border-radius: 0 0 27px 27px;
  position: absolute;
  bottom: 20%;
  right: 30%;
  transform-origin: -15% 0;
`

export const BoxTwo = styled.div`
  width: 35%;
  height: 20%;
  background: transparent;
  border-radius: 50%;
  border-top: 0.7rem solid #333;
  border-left: 0.7rem solid transparent;
  position: absolute;
  bottom: 55%;
  right: 80%;
  transform: rotate(20deg) rotateX(0deg) scale(1.3, 0.9);
`

export const BoxInnerTwo = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 24%;
  transform: rotateX(85deg);
  animation: loading-1 2.1s ease-in-out infinite;

  @keyframes loading-1 {
    0% {
      top: 24%;
      transform: rotateX(85deg);
    }
    25% {
      top: 10%;
      transform: rotateX(0deg);
    }
    50% {
      top: 30%;
      transform: rotateX(85deg);
    }
    75% {
      transform: rotateX(0deg);
    }
    100% {
      transform: rotateX(85deg);
    }
  }
`

export const BoxThree = styled.div`
  width: 35%;
  height: 40%;
  background: #333;
  border-radius: 50%;
  box-shadow: 0 0 3px 0 #333;
  position: absolute;
  bottom: 46%;
  right: 40%;
  transform-origin: -32% 0;
  animation: loading-4 2.1s ease-in-out infinite;

  @keyframes loading-4 {
    0% {
      bottom: 26%;
      transform: rotate(0deg);
    }
    10% {
      bottom: 40%;
    }
    50% {
      bottom: 26%;
      transform: rotate(-190deg);
    }
    80% {
      bottom: 40%;
    }
    100% {
      bottom: 50%;
      transform: rotate(0deg);
    }
  }
`
