import { Container, Box, BoxInnerOne, BoxInnerTwo, BoxOne, BoxThree, BoxTwo } from './styles'

const Loader = () => (
  <Container>
    <Box>
      <BoxInnerOne>
        <BoxOne></BoxOne>
        <BoxTwo></BoxTwo>
      </BoxInnerOne>
      <BoxInnerTwo>
        <BoxThree></BoxThree>
      </BoxInnerTwo>
    </Box>
  </Container>
)

export default Loader
