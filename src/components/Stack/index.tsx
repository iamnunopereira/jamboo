import styled from '@emotion/styled'
import { StackPropTypes } from './types'

const getCSSVariable = (spacer: string) => `var(--spacers-${spacer})`

const Stack = styled.div<StackPropTypes>`
  display: flex;
  flex-direction: ${({ direction }) => (direction === 'horizontal' ? 'column' : 'row')};
  gap: ${({ gap }) => (gap ? getCSSVariable(gap) : 0)};
`

export default Stack
