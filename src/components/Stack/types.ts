export type StackPropTypes = {
  direction: 'horizontal' | 'vertical'
  gap?: string
}
