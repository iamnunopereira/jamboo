import Head from 'next/head'
import type { MetaProps } from './types'

export const MetadataProps = {
  title: 'Restaurante Jamboo',
  description: 'Restaurante Jamboo Web Application',
  ogImage: 'Default Layout OG Image',
  ogUrl: 'Default Layout OG URL',
}

const Metadata = ({
  title = MetadataProps.title,
  description = MetadataProps.description,
  ogImage = MetadataProps.ogImage,
  ogUrl = MetadataProps.ogUrl,
}: MetaProps) => {
  return (
    <Head>
      <title>{title}</title>
      <link rel="icon" href="/favicon.ico" />
      <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
      <link rel="apple-touch-icon" sizes="180x180" href="/favicon.ico" />
      {/* <meta name="theme-color" content="#7b46f6" /> */}

      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />

      <meta itemProp="name" content={title} />
      <meta itemProp="description" content={description} />
      <meta itemProp="image" content={ogImage} />
      <meta name="description" content={description} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:url" content={ogUrl} />
      <meta property="og:image" content={ogImage} />
      <meta property="og:type" content="website" />

      {/*
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@twitter-site" />
      <meta name="twitter:creator" content="@twitter-creator" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={ogImage} />
      */}
    </Head>
  )
}

export default Metadata
