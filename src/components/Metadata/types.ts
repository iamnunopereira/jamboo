export type MetaProps = {
  title?: string
  description?: string
  ogUrl?: string
  ogImage?: string
}
