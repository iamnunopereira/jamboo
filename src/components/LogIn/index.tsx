import Stack from '@/components/Stack'
import Input from '@/components/Input'
import { auth } from '@/lib/firebaseClient'
import { FirebaseError } from 'firebase/app'
import { useState, FormEvent } from 'react'
import { signInWithEmailAndPassword } from 'firebase/auth'

const LogIn = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const signIn = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    try {
      await signInWithEmailAndPassword(auth, email, password)
    } catch (error: FirebaseError | unknown) {
      if (error instanceof FirebaseError) {
        console.log({ errorCode: error.code, errorMessage: error.message })
      }
    }
  }

  return (
    <div>
      <h1>Entrar</h1>
      <form onSubmit={(e) => signIn(e)} className="form">
        <Stack direction="horizontal" gap="c12">
          <Input
            onChange={(value) => setEmail(value)}
            id="sign_in_email"
            label="E-Mail"
            type="email"
            placeholder="E-Mail"
            required={true}
          />
          <Input
            onChange={(value) => setPassword(value)}
            id="sign_in_password"
            label="Password"
            type="password"
            placeholder="Password"
            required={true}
          />
          <button type="submit">Entrar</button>
        </Stack>
      </form>
    </div>
  )
}

export default LogIn
