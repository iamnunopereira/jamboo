import Stack from '@/components/Stack'
import Input from '@/components/Input'
import { auth } from '@/lib/firebaseClient'
import { FirebaseError } from 'firebase/app'
import { useState, FormEvent } from 'react'
import { createUserWithEmailAndPassword } from 'firebase/auth'

const SignIn = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const signUp = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    if (password === confirmPassword) {
      try {
        await createUserWithEmailAndPassword(auth, email, password)
      } catch (error: FirebaseError | unknown) {
        if (error instanceof FirebaseError) {
          const errorCode = error.code
          const errorMessage = error.message

          console.log({ errorCode, errorMessage })
        }
      }
    } else {
      console.log('Password dont Match')
    }
  }

  return (
    <div>
      <h1>Cadastrar</h1>
      <form onSubmit={(e) => signUp(e)} className="form">
        <Stack direction="horizontal" gap="c12">
          <Input
            onChange={(value) => setEmail(value)}
            id="sign_up_email"
            label="E-Mail"
            type="email"
            placeholder="E-Mail"
            required={true}
          />
          <Input
            onChange={(value) => setPassword(value)}
            id="sign_up_password"
            label="Password"
            type="password"
            placeholder="Password"
            required={true}
          />
          <Input
            onChange={(value) => setConfirmPassword(value)}
            id="sign_up_confirm_password"
            label="Password"
            type="password"
            placeholder="Repete a Password"
            required={true}
          />
          <button type="submit">Cadastrar</button>
        </Stack>
      </form>
    </div>
  )
}

export default SignIn
