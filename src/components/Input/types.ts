export type InputPropTypes = {
  onChange: (e: string) => void
  id: string
  label: string
  required: boolean
  type: string
  placeholder: string
}
