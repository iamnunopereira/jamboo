import Input from './'
import { render } from '@testing-library/react'

import { inputMocks } from '@/__mocks__'

describe('<Input /> spec', () => {
  it('Should match snapshot', () => {
    const { container } = render(<Input {...inputMocks} />)

    expect(container).toMatchSnapshot()
  })
})
