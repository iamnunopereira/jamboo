import { Container } from './styles'
import { InputPropTypes } from './types'

const Input = ({ id, label, required, type, placeholder, onChange }: InputPropTypes) => (
  <Container>
    <label htmlFor={id}>{label}</label>
    <input
      onChange={(e) => onChange(e.target.value)}
      required={required}
      type={type}
      name={id}
      placeholder={placeholder}
    />
  </Container>
)

export default Input
