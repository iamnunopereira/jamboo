import styled from '@emotion/styled'

export const Container = styled.div`
  display: flex;
  gap: var(--spacers-c10);
`
