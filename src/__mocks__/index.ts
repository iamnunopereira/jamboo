export const inputMocks = {
  id: 'test',
  label: 'test',
  required: true,
  type: 'text',
  placeholder: 'test',
  onChange: () => null,
}
