import '@csstools/normalize.css'
import '@/styles/variables.css'

import Fallback from './fallback'
import posthog from 'posthog-js'
import ErrorBoundary from '@/components/ErrorBoundary'
import DefaultLayout from '@/layouts/DefaultLayout'
import { AppProps } from 'next/app'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { GlobalStyles } from '@/styles/GlobalStyles'
import { PostHogProvider } from 'posthog-js/react'
import { AuthContextProvider } from '@/contexts/AuthContext'
import { SettingsContextProvider } from '@/contexts/SettingsContext'

if (typeof window !== 'undefined') {
  posthog.init(process.env.NEXT_PUBLIC_POSTHOG_KEY!, {
    api_host: process.env.NEXT_PUBLIC_POSTHOG_HOST,
    disable_session_recording: true,
    autocapture: false,
    loaded: (posthog) => {
      if (process.env.NODE_ENV === 'development') posthog.debug()
    },
  })
}

const FallbackPage = () => <Fallback />

export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter()

  useEffect(() => {
    const handleRouteChange = () => posthog.capture('$pageview')

    router.events.on('routeChangeComplete', handleRouteChange)

    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [])

  return (
    <>
      <ErrorBoundary fallback={FallbackPage}>
        <PostHogProvider client={posthog}>
          <SettingsContextProvider>
            <AuthContextProvider>
              <DefaultLayout>
                <Component {...pageProps} />
              </DefaultLayout>
            </AuthContextProvider>
          </SettingsContextProvider>
        </PostHogProvider>
      </ErrorBoundary>
      <GlobalStyles />
    </>
  )
}
