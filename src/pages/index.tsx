import Image from 'next/image'
import styled from '@emotion/styled'
import { posthogClient } from '@/lib/posthogClient'
import { getServerUser } from '@/utils'
import { GetServerSidePropsContext } from 'next/types'

import cta from '../../public/003.webp'

const Container = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background: rgb(156, 155, 141);
  background: radial-gradient(circle, rgba(156, 155, 141, 1) 39%, rgba(214, 213, 196, 1) 100%);
`

const Home = () => {
  return (
    <Container>
      <Image
        priority
        src={cta}
        width={650}
        height={580}
        alt=""
        style={{ filter: 'drop-shadow(.3rem .3rem 2rem var(--color-base-black))' }}
      />
    </Container>
  )
}

export async function getServerSideProps(ctx: GetServerSidePropsContext) {
  const { isLoggedIn, user, loginErrorOn } = await getServerUser(ctx)

  const flags = await posthogClient.getAllFlags('ssr flags')

  return {
    props: {
      isLoggedIn,
      user,
      loginErrorOn,
      flags,
    },
  }
}

export default Home
