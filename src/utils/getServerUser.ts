import nookies from 'nookies'
import { adminSDK } from '@/lib/firebaseAdmin'
import { GetServerSidePropsContext } from 'next/types'

export const getServerUser = async (ctx: GetServerSidePropsContext) => {
  const cookies = nookies.get(ctx)

  if (!cookies.token) {
    return {
      user: null,
      isLoggedIn: false,
      loginErrorOn: 'verify cookie',
    }
  }

  try {
    const token = await adminSDK.auth().verifyIdToken(cookies.token)

    if (!token) {
      return {
        user: null,
        isLoggedIn: false,
        loginErrorOn: 'verify token',
      }
    }

    const { uid } = token

    const user = await adminSDK.auth().getUser(uid)

    return {
      user: JSON.stringify(user),
      isLoggedIn: true,
      loginErrorOn: null,
    }
  } catch (error) {
    return {
      user: null,
      isLoggedIn: false,
      loginErrorOn: 'verify user',
    }
  }
}
