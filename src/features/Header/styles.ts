import styled from '@emotion/styled'

export const Container = styled.header`
  width: 30rem;
  height: 100vh;
  padding: var(--spacers-c24);
  color: var(--color-base-white);
  background-color: var(--color-base-black);
  z-index: var(--stack-order-medium);
  box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px;

  @media only screen and (max-width: 1024px) {
    width: 100vw;
    height: auto;
    padding: var(--spacers-c14) 0;
  }
`

export const LogoContainer = styled.div`
  display: flex;
  justify-content: center;
`

export const Nav = styled.nav``
