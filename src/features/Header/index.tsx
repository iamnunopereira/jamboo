import Image from 'next/image'
import Fallback from './Fallback'
import ErrorBoundary from '@/components/ErrorBoundary'
import { Container, Nav, LogoContainer } from './styles'

import Logo from '../../../public/Logo.png'

const Header = () => {
  return (
    <ErrorBoundary fallback={Fallback}>
      <Container>
        <LogoContainer>
          <Image src={Logo} alt="Jamboo Logo" height={70} width={240} />
        </LogoContainer>
        <Nav></Nav>
      </Container>
    </ErrorBoundary>
  )
}

export default Header
