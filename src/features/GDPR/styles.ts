import { motion } from 'framer-motion'
import styled from '@emotion/styled'

export const Container = styled(motion.div)`
  position: fixed;
  display: flex;
  align-items: center;
  gap: var(--spacers-c14);
  bottom: var(--spacers-c24);
  right: var(--spacers-c24);
  z-index: var(--stack-order-low);
  color: var(--color-base-white);
  background-color: var(--color-base-black);
  padding: var(--spacers-c14) var(--spacers-c24);
  border-radius: var(--spacers-c12);
`

export const Button = styled.button`
  border: none;
  overflow: visible;
  padding: var(--spacers-c4) var(--spacers-c8);
  border-radius: var(--spacers-c8);
  background: var(--color-success-one);
  outline: none;
  color: inherit;
  font: inherit;
  line-height: normal;
  cursor: pointer;
`
