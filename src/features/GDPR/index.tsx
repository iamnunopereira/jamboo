import useTranslation from 'next-translate/useTranslation'
import { useEffect } from 'react'
import { AnimatePresence } from 'framer-motion'
import { Container, Button } from './styles'
import { useSettingsContext } from '@/hooks/useSettingsContext'
import { parseCookies, setCookie } from 'nookies'

const GDPR = () => {
  const { t } = useTranslation('common')
  const { acceptedGDPR } = parseCookies()
  const { gdpr, toggleGdpr } = useSettingsContext()

  useEffect(() => {
    if (acceptedGDPR && gdpr) {
      toggleGdpr!(false)
    }
  }, [])

  const handleAccept = () => {
    setCookie(null, 'acceptedGDPR', 'true', {
      maxAge: 30 * 24 * 60 * 60,
      path: '/',
    })

    toggleGdpr!(false)
  }

  return (
    <AnimatePresence>
      {gdpr && (
        <Container initial={{ y: 200 }} animate={{ y: 0, opacity: 1 }} exit={{ y: 200, opacity: 0 }}>
          <p>{t('gdpr_message')}</p>
          <Button onClick={() => handleAccept()}>{t('accept')}</Button>
        </Container>
      )}
    </AnimatePresence>
  )
}

export default GDPR
