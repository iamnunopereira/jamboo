import type { ReactNode } from 'react'

export type PropsWithChildren = {
  children: ReactNode
}

export type HomePropsTypes = {
  isLoggedIn: boolean
  loginErrorOn?: string
  user?: string
}
